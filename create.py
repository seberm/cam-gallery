#!/usr/bin/env python3

import os
import sys
from glob import glob
import argparse
import shutil

from jinja2 import Environment, FileSystemLoader, select_autoescape
from PIL import Image


_CUR_DIR = os.path.dirname(os.path.realpath(__file__))


SCRIPTS = [
    os.path.join(_CUR_DIR, "node_modules/jquery/dist/jquery.min.js"),
    os.path.join(_CUR_DIR, "node_modules/lightgallery/dist/js/lightgallery.min.js"),
    os.path.join(_CUR_DIR, "node_modules/lg-thumbnail/dist/lg-thumbnail.min.js"),
]

STYLES = [
    os.path.join(_CUR_DIR, "node_modules/lightgallery/dist/css/lightgallery.min.css"),
]

TEMPLATE_INDEX = os.path.join(_CUR_DIR, "index.html.jinja2")

THUMBNAILS_DIR = "min"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("directory")
    parser.add_argument("-t", "--thumbnail", action="store_true", help="Create thumbnails for all images")
    args = parser.parse_args()

    # Pairs (image, thumnail,)
    images = [
        {
            "path": i,
            "thumbnail": i,
        } for i in glob(os.path.join(args.directory, "*.jpg"))
    ]

    if not images:
        print(f"Directory {args.directory} does not cointain images! Exiting ...")
        sys.exit(99)

    if args.thumbnail:
        try:
            os.mkdir(os.path.join(args.directory, THUMBNAILS_DIR))
        except OSError as e:
            print(e)
            print(f"Cannot create directory for thumbnails ({args.directory}")

        for image in images:
            try:
                im = Image.open(image["path"])
                im.thumbnail((200, 200, ))

                thumbnail_basename = os.path.basename(image["path"].rstrip(".jpg") + ".min.jpg")
                thumbnail_filename = os.path.join(args.directory, THUMBNAILS_DIR, thumbnail_basename)
                im.save(thumbnail_filename, "JPEG")
                image["thumbnail"] = thumbnail_basename
            except IOError as e:
                print(e)
                print(f"Cannot create thumbnail for {image}")

    context = {
        "scripts": [os.path.basename(s) for s in SCRIPTS],
        "styles": [os.path.basename(s) for s in STYLES],
        "images": [{"path": os.path.basename(i["path"]), "thumbnail": os.path.join(THUMBNAILS_DIR, i["thumbnail"]), } for i in images],
    }

    env = Environment(
            loader=FileSystemLoader(os.path.join(_CUR_DIR, "templates")),
            autoescape=select_autoescape(["html", "xml"]),
    )
    template = env.get_template("index.html.jinja2")

    # Write the index file into dest folder
    with open(os.path.join(args.directory, "index.html"), "w") as fd:
        fd.write(template.render(**context))

    # Copy all the scripts and CSS styles
    for src_file in SCRIPTS + STYLES:
        dest_file = os.path.join(args.directory, os.path.basename(src_file))
        shutil.copy(src_file, dest_file)


if __name__ == "__main__":
    main()
